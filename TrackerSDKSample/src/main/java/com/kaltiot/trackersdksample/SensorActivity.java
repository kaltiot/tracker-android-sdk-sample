package com.kaltiot.trackersdksample;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.util.Log;
import android.content.res.Configuration;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.method.ScrollingMovementMethod;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;

import com.kaltiot.smarttracker.TrackerServiceApi;
import com.kaltiot.smarttracker.TrackerServiceApiCallbacks;

/* SensorActivity is sample code demonstrating the use of Kaltiot Smart Tracker SDK.
   SensorActivity needs to:
   1) connect to TrackerServiceApi
   2) starts the service
   3) bind to the service (optional)
   4) enable BLE scanning

   The service will send data to the UI through a set of callbacks that needs to be implemented:
     state_callback        - SDK-to-Cloud connection state
     rid_callback          - Unique RID for this application
     appid_callback        - Application ID for our organisation
     value_callback        - GPS and BLE scan values
     notification_callback - Notification received from backend
     geofence_callback     - Geofence area configured
     closeui_callback      - Action to close this UI silently
*/
   
public class SensorActivity extends Activity
                            implements TrackerServiceApiCallbacks, OnTouchListener {

    public static final String TAG = "SmartTrackerSDKSample";
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 111;

    View mFullView;                     // For listening touch on whole display
    ImageView mImageView;               // Placeholder for showing image
    TextView mTextInfo, mTextConnection, mTextRid, mTextValue;
    EditText mTextNotification;         // Scrollable text view
    String mGoogleAccount = "AndroidTrackerCustomerId";  // Customer ID for this app

    private TrackerServiceApi service = null;   // Connection to TrackerService
    private int mMaxNotificationLines = 50; // Text lines to show in mTextNotification
    private BufferedWriter mFileWriter = null;
    private String mAppId = null;
    private String mRid = null;
    private String mState = null;
    private boolean mFineLocationPermission = false;

    // Storing the sensor values
    private float mTouchX = 0.0f, mTouchY = 0.0f;
    private String mPublishStr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        
        mFullView = (View) findViewById(R.id.fullView);
        mFullView.setOnTouchListener(this);

        mTextInfo = (TextView) findViewById(R.id.infotext);
        getGoogleAccount();
        mTextInfo.append("Android: "+Build.VERSION.RELEASE);
        mTextConnection = (TextView) findViewById(R.id.connectiontext);
        mTextRid = (TextView) findViewById(R.id.ridtext);
        mTextValue = (TextView) findViewById(R.id.valuetext);
        mImageView = (ImageView) findViewById(R.id.imageview);
        mTextNotification = (EditText) findViewById(R.id.notificationtext);
        mTextNotification.setMovementMethod(new ScrollingMovementMethod());
        mTextConnection.setText("offline");
        mTextValue.setText(getCurrentTimeString());

        // Step 1: Create TrackerServiceApi instance
        // You can give unique id for this app
        service = new TrackerServiceApi(TAG, mGoogleAccount, this,
                                        this.getPackageName(),
                                        this.getClass().getName(), this);

        // Step 2: Start the background service
        service.startService();
        // (optional) Step 3: Bind to the service to receive data
        service.doBindService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        
        // Reconnect if Service connection has died while we were on background
        if (!service.isServiceConnected())
            service.startService();

        if (!service.isServiceBound())
            service.doBindService();

        /* Checking for permissions at run time needed when targeting API 23 or later. */
        if (ContextCompat.checkSelfPermission(this,
                      android.Manifest.permission.ACCESS_FINE_LOCATION)
                      != PackageManager.PERMISSION_GRANTED) {
              ActivityCompat.requestPermissions(this,
                      new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                      MY_PERMISSION_ACCESS_FINE_LOCATION);
        }

    }


    // Get permission result
    @Override
    public void onRequestPermissionsResult(int requestCode,
            String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    Log.i(TAG, "Permission for fine location granted.");
                    mFineLocationPermission = true;
                    startScanWhenReady();
                } else {
                    // permission was denied
                    Log.i(TAG, "Permission for fine location denied.");
                    mFineLocationPermission = false;
                }
                return;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) shutdown();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isFinishing()) shutdown();
    }

    @Override
    protected void onDestroy() {
        shutdown();
        try {
            mFileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void shutdown() {
        if (service.isServiceBound())
            service.doUnbindService();
    }

    public void publish(String text) {
        // Example how to publish String to the cloud service
        if (!text.equals(mPublishStr))
            service.publishMessage(text, TrackerServiceApi.PAYLOAD_STRING);
        mPublishStr = text;
    }

    // Receive touch events, store the latest touch point 
    public boolean onTouch(View v, MotionEvent ev) {
        int pointerCount = ev.getPointerCount();
        Log.i(TAG, "Touch: " + pointerCount);
        if (pointerCount == 0) return false;
        mTouchX = ev.getX(pointerCount-1)/v.getWidth();
        mTouchY = ev.getY(pointerCount-1)/v.getHeight();
        publish(mTouchX+","+mTouchY);
        return true;
    }

    protected void getGoogleAccount() {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        Account[] list = manager.getAccountsByType("com.google");
        if (list.length > 0) {
            mGoogleAccount = list[0].name;
        }
        mTextInfo.append("Customer ID: " + mGoogleAccount + "\n");
    }

    public String getCurrentTimeString() {
        Calendar c = Calendar.getInstance(); 
        return c.get(Calendar.HOUR_OF_DAY)+"."+
               c.get(Calendar.MINUTE)+":"+
               c.get(Calendar.SECOND);
    }

    public String getCurrentTimeDateString() {
        Calendar c = Calendar.getInstance();
        return "time "+c.get(Calendar.HOUR_OF_DAY)+"."+
               c.get(Calendar.MINUTE)+":"+
               c.get(Calendar.SECOND)+" date "+
               c.get(Calendar.DATE)+"."+
               (c.get(Calendar.MONTH)+1)+".";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startScanWhenReady() {
        // Step 4: Enable background scanning
        // Once the SDK connects to cloud service and user has
        // allowed location access start the bluetooth scanning
        if (mState.equals("online") && mFineLocationPermission)
            service.setScanEnable(true);
    }

    // Callback methods from TrackerServiceApi
    // SDK-to-Cloud connection state
    public void state_callback(String state) {
        mState = state;
        Log.i(TAG, "State: " + state);
        updateConnectionText(state);

        startScanWhenReady();
    }

    // Receive a unique Resource ID for this app
    public void rid_callback(final String rid) {
        mRid = rid;
        Log.i(TAG, "Rid: " + rid);
        updateIdText();
    }

    // Receive the application ID of our organisation
    public void appid_callback(String appid) {
        mAppId = appid;
        Log.i(TAG, "Appid: " + appid);
        updateIdText();
    }

    // Receive information from the background service:
    //  - GPS latitude, longitude and accuracy
    //  - Array of beacons found in BLE scan
    //  - Message
    public void value_callback(int accuracy, double lat, double lon,
                               ArrayList<String> beacons, String message) {
        int count = 0;
        if (beacons != null) count = beacons.size();
        final String value = getCurrentTimeString() + " " + lat + " " + lon +
                             " acc: " + accuracy + " beacons: " + count;
        Log.i(TAG, "Value: " + value);
        updateValueText(value);

        addNotificationText(message);
    }

    // Receiving notifications sent to this application from cloud service
    public void notification_callback(String address, String str, int payload_length,
                int payload_type, String msg_id) {

        Log.i(TAG, "Notification: " + str);

        addNotificationText(str);
    }

    // Simple UI to display the messages we have received with limited history
    public void addNotificationText(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                /* Adding text to notification view, remove the
                   oldest lines to limit the amount of lines. */
                if (mTextNotification != null && mTextNotification.getLayout() != null &&
                    mTextNotification.getText() != null) {
                    String oldtext = mTextNotification.getText().toString();
                    String newtext = "\n" + getCurrentTimeString() + ": " + str;
                    Layout textLayout = mTextNotification.getLayout();
                    if (textLayout.getLineCount() > mMaxNotificationLines) {
                        int remove = textLayout.getLineCount() -
                                     mMaxNotificationLines + 1;
                        int removeChars = textLayout.getLineEnd(remove);
                        if (removeChars < oldtext.length())
                            oldtext = oldtext.substring(removeChars, oldtext.length());
                    }
                    mTextNotification.setText(oldtext + newtext);
                }
            }
        });
    }

    public void updateIdText() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (mAppId == null)
                    mTextRid.setText("Device RID "+mRid);
                else
                    mTextRid.setText("Application ID "+mAppId+"\nDevice RID "+mRid);
            }
        });
    }

    public void updateConnectionText(final String state) {
        runOnUiThread(new Runnable() {
            public void run() {
                mTextConnection.setText(state);
            }
        });
    }

    public void updateValueText(final String value) {
        runOnUiThread(new Runnable() {
            public void run() {
                mTextValue.setText(value);
            }
        });
    }

    // Service can be configured to activate inside a geofence 
    public void geofence_callback(String lat, String lon, String rad) {}

    // Service would like to close the UI without user interaction 
    public void closeui_callback() {
        runOnUiThread(new Runnable() {
            public void run() {
                finish();
            }
        });
    }
    
}
