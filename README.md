# README #

This is sample code demonstrating the use of Kaltiot Smart Tracker Android SDK.

## Kaltiot Smart Tracker SDK ##

* The SDK implements an Android Service that runs as a background process.
* It scans for nearby beacons, receives GPS location updates and transmits scan results to cloud service.
* See SensorActivity.java for example code on how to start the Service

## Known issues ##

* KaltiotSmartTracker application must be removed first. SDK and app don't work together yet.

## How to build? ##

* Install android-studio
* Install android SDK, minimum API 18
* Build your personal Android SDK from Console -> Downloads -> Android SDK
* Download your Android SDK to KaltiotSmartGateway/KaltiotSmartGateway.aar
* Get the Android Tracker SDK and copy it to KaltiotSmartTracker/KaltiotSmartTracker.aar
* Android Studio -> Open project -> tracker-android-sdk-sample
* Compile and Run

